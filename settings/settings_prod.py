"""
Django settings for production
"""
import django_heroku

from .settings import *

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'CoUk`IOQ}25Dar&y(`0Qp=Fh$)f;/w2Tr`/*;E=kb=74Lmo"R7PA}0&R@BnK,m'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
DEBUG_PROPAGATE_EXCEPTIONS = True

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

ALLOWED_HOSTS = ['*']

CORS_ORIGIN_WHITELIST = (
    'https://dikinaf-sport-app-backend.herokuapp.com',
    'https://dikinaf-sport-app.herokuapp.com',
    'http://localhost:3000',
)

APP_SITE_URL = 'https://dikinaf-sport-app.herokuapp.com'

# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('DATABASE_NAME'),
        'USER': os.environ.get('DATABASE_USER'),
        'PASSWORD': os.environ.get('DATABASE_PASSWORD'),
        'HOST': os.environ.get('DATABASE_HOST'),
        'PORT': os.environ.get('DATABASE_PORT', 5432),
    }
}

django_heroku.settings(locals())
