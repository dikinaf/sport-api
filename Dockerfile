FROM python:3.10.5-buster

ENV PROJECT_NAME target-edu-api
ENV PROJECT_PATH /app

RUN apt-get update \
    && apt-get install -y binutils libproj-dev gdal-bin

RUN mkdir -p $PROJECT_PATH

WORKDIR $PROJECT_PATH

RUN mkdir -p $PROJECT_PATH/staticfiles
RUN mkdir -p $PROJECT_PATH/static

# Copy requirements for catch
ADD ./requirements.txt $PROJECT_PATH
RUN pip3 install -r requirements.txt

# Copy project files
ADD . $PROJECT_PATH

# Install assets
RUN python manage.py collectstatic --noinput --clear

VOLUME $PROJECT_PATH/media
VOLUME $PROJECT_PATH/staticfiles
