from django.urls import path, include

urlpatterns = [
    path('game/', include(('apps.game.urls', 'apps.game'), 'game')),
    path('auth/', include(('apps.custom_auth.urls', 'apps.custom_auth'),
                          'custom_auth')),
]
