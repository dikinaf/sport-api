from django.urls import path, include
from rest_framework import routers

from apps.custom_auth.views import AuthUserView, UserViewSet, MeUserView

app_name = 'apps.custom_auth'

router = routers.DefaultRouter()
router.register('register', UserViewSet, basename='register')

urlpatterns = [
    path('', include(router.urls)),
    path('login/', AuthUserView.as_view(), name='login'),
    path('me/', MeUserView.as_view(), name='me'),
]
