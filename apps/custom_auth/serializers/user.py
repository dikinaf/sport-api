from django.contrib.auth.models import User

from rest_framework import serializers


class MeUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id', 'username', 'first_name', 'last_name',
            'is_staff', 'email', 'date_joined', 'last_login'
        )

    def to_representation(self, instance):
        return super().to_representation(instance)


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = (
            'id', 'username', 'last_name', 'first_name', 'password', 'email',
        )

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        new_user = User(**validated_data)
        if password:
            new_user.set_password(password)
        new_user.save()
        return new_user
