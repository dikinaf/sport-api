from rest_framework import viewsets, mixins

from django.contrib.auth.models import User
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import AllowAny

from apps.custom_auth.serializers import UserSerializer, MeUserSerializer


class MeUserView(RetrieveAPIView):
    queryset = User.objects
    serializer_class = MeUserSerializer

    def get_object(self):
        return self.request.user


class UserViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)
