from rest_framework import serializers
from apps.game.models import Game
from apps.game.serializers.sport_type import SportTypeSerializer


class GameSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        self.fields['sport_type'] = SportTypeSerializer()
        return super().to_representation(instance)

    class Meta:
        model = Game
        fields = '__all__'
