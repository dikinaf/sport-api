from django.contrib import admin
from .models import Game, SportType

admin.site.register(Game)
admin.site.register(SportType)
