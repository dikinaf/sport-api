from django.db import models

from apps.game.models.sport_type import SportType


class Game(models.Model):
    name = models.CharField(max_length=255)
    date = models.DateTimeField()
    sport_type = models.ForeignKey(SportType, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
