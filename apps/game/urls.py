from django.urls import path, include
from rest_framework import routers

from apps.game.views import SportTypeViewSet, GameViewSet

app_name = 'apps.game'

router = routers.DefaultRouter()
router.register(r'games', GameViewSet, basename='game')
router.register(r'sport-types', SportTypeViewSet, basename='sport-type')

urlpatterns = [
    path('', include(router.urls)),
]
