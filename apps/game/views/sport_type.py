from rest_framework import viewsets, mixins

from apps.game.models import SportType
from apps.game.serializers import SportTypeSerializer


class SportTypeViewSet(mixins.ListModelMixin,
                       mixins.RetrieveModelMixin,
                       viewsets.GenericViewSet):
    queryset = SportType.objects.all()
    serializer_class = SportTypeSerializer
    search_fields = ('name',)
