from rest_framework import viewsets, mixins

from apps.game.models import Game
from apps.game.serializers import GameSerializer


class GameViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    search_fields = ('name', )
    filterset_fields = ['sport_type', ]
