from django.conf import settings
from django.contrib import admin


class CoreAdminSite(admin.AdminSite):
    site_url = settings.APP_SITE_URL
